﻿using System;
// класс игрока, у которого есть здоровье и координаты на поле
class Player
{
    public int oldX, oldY;
    public int x, y;
    public int lives = 10;

    public void Reset()
    {
        x = 0;
        y = 0;
        oldX = 0;
        oldY = 0;
        lives = 10;
    }
}

// класс игры, который создаём на старте
class Game
{

    Player player;
    Random random;

    // число бомб
    const int bombs = 10;

    // размер поля
    const int size = 10;
    int[,] field = new int[size, size];

    public Game()
    {
        random = new Random();
        player = new Player();
    }

    // заполнение поля при старте новой игры
    public void Start()
    {
        Console.WriteLine("-----Новая игра-----");
        player.Reset();

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                field[i, j] = 0;
            }
        }

        field[player.x, player.y] = 2;
        field[size - 1, size - 1] = 3;

        SetBombs();
    }

    // считываем клавишу и делаем ход
    public void GetKey(char key)
    {
        player.oldX = player.x;
        player.oldY = player.y;
        switch (key)
        {
            case 'w':
                if (player.x > 0)
                    player.x--;
                break;
            case 's':
                if (player.x < size - 1)
                    player.x++;
                break;
            case 'a':
                if (player.y > 0)
                    player.y--;
                break;
            case 'd':
                if (player.y < size - 1)
                    player.y++;
                break;
        }
        Next();
    }

    // перезаписываем ячейки массива после перемещения игрока
    public void Next()
    {
        field[player.oldX, player.oldY] = 0;

        switch (field[player.x, player.y])
        {
            case 1:
                Shot();
                break;
            case 3:
                Win();
                break;
        }
        field[player.x, player.y] = 2;

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                switch (field[i, j])
                {
                    case 2:
                        Console.Write('P');
                        break;
                    case 3:
                        Console.Write('Q');
                        break;
                    default:
                        Console.Write('-');
                        break;
                }
            }
            Console.WriteLine();
        }
        Console.WriteLine("Здоровье: " + player.lives);
    }

    // заполнение поля бомбами
    public void SetBombs()
    {
        int i = 0;
        while (i < bombs)
        {
            int x = random.Next(0, size);
            int y = random.Next(0, size);

            if (x == 0 && y == 0)
                continue;
            if (x == size - 1 && y == size - 1)
                continue;

            field[x, y] = 1;
            i++;
        }
    }

    // случайный урон от бомбы
    private void Shot()
    {
        int damage = random.Next(1, 10);
        if (player.lives > damage)
            player.lives -= damage;
        else
            player.lives = 0;

        Console.WriteLine("Вы наткнулись на мину");

        if (player.lives == 0)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        Console.WriteLine("Вы проиграли");
        Start();
    }

    private void Win()
    {
        Console.WriteLine("Вы выиграли");
        Start();
    }
}

class HelloWorld
{
    static void Main()
    {
        Game game = new Game();
        game.Start();
        game.Next();

        while (true)
        {
            try
            {
                game.GetKey(char.Parse(Console.ReadLine()));
            }
            catch
            {
                Console.WriteLine("Неверный ввод. Повторите попытку.");
            }
        }
    }
}
